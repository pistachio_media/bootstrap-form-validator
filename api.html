---
layout:           default
title:            API
permalink:        /api/
previous_section: /examples/
next_section:     /developing/
comments:         true
sections:
    - title:  defaultSubmit
      anchor: default-submit
    - title:  disableSubmitButtons
      anchor: disable-submit-buttons
    - title:  enableFieldValidators
      anchor: enable-field-validators
    - title:  getFieldElements
      anchor: get-field-elements
    - title:  isValid
      anchor: is-valid
    - title:  resetForm
      anchor: reset-form
    - title:  setLiveMode
      anchor: set-live-mode
    - title:  updateElementStatus
      anchor: update-element-status
    - title:  updateStatus
      anchor: update-status
    - title:  validate
      anchor: validate
    - title:  validateField
      anchor: validate-field
---

<section class="doc-heading">
    <h1>API</h1>
    {% include edit-button.html %}
</section>

<p>This page describes the public methods provided by the plugin.</p>

<section>
    <p>After initializing the form with the plugin using <code>$(form).bootstrapValidator(options)</code>, there are two ways to call the plugin method:</p>
    <pre><code class="javascript">// Get plugin instance
var bootstrapValidator = $(form).data('bootstrapValidator');
// and then call method
bootstrapValidator.methodName(parameters)
</code></pre>
    <p>or:</p>
    <pre><code class="javascript">$(form).bootstrapValidator(methodName, parameters);</code></pre>
    <p>The first way mostly returns the <code>BootstrapValidator</code> instance, meanwhile the second one always returns the jQuery object representing the form.</p>
    <p>So, it's possible to chain methods as below:</p>
    <pre><code class="javascript">// The first way
$(form)
    .data('bootstrapValidator')
    .updateStatus('birthday', 'NOT_VALIDATED')
    .validateField('birthday');

// The second one
$(form)
    .bootstrapValidator('updateStatus', 'birthday', 'NOT_VALIDATED')
    .bootstrapValidator('validateField', 'birthday');</code></pre>
</section>

<section id="default-submit">
    <h2>defaultSubmit</h2>

    <p><code>defaultSubmit()</code> &mdash; Submit the form using default submission.</p>
    <p>It also does not perform any validations when submitting the form. It might be used when you want to submit the form right inside the <a href="/settings/#submit-handler">custom submit handler</a>.</p>
</section>

<section id="disable-submit-buttons">
    <h2>disableSubmitButtons</h2>

    <p><code>disableSubmitButtons(disabled)</code> &mdash; Disable or enable the submit buttons</p>

    <table class="table table-condensed">
        <thead>
            <tr>
                <th>Parameter</th>
                <th>Type</th>
                <th>Description</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>disabled</code></td>
                <td>Boolean</td>
                <td>Can be <code>true</code> or <code>false</code></td>
            </tr>
        </tbody>
    </table>
</section>

<section id="enable-field-validators">
    <h2>enableFieldValidators</h2>

    <p><code>enableFieldValidators(field, enabled)</code> &mdash; Enable/disable all validators to given field</p>

    <table class="table table-condensed">
        <thead>
            <tr>
                <th>Parameter</th>
                <th>Type</th>
                <th>Description</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>field</code></td>
                <td>String</td>
                <td>The field name</td>
            </tr>
            <tr>
                <td><code>enabled</code></td>
                <td>Boolean</td>
                <td>If <code>true</code>, enable field validators. If <code>false</code>, disable field validators</td>
            </tr>
        </tbody>
    </table>
</section>

<section id="get-field-elements">
    <h2>getFieldElements</h2>

    <p>
        <code>getFieldElements(field)</code> &mdash; Retrieve the field elements by given name.<br/>
        Returns array of jQuery element representing the field, or <code>null</code> if the fields are not found.
    </p>

    <table class="table table-condensed">
        <thead>
            <tr>
                <th>Parameter</th>
                <th>Type</th>
                <th>Description</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>field</code></td>
                <td>String</td>
                <td>The field name</td>
            </tr>
        </tbody>
    </table>
</section>

<section id="is-valid">
    <h2>isValid</h2>

    <p><code>isValid()</code> &mdash; Returns <code>true</code> if all form fields are valid. Otherwise, returns <code>false</code>.</p>
    <p>Ensure that the <a href="#validate">validate</a> method is already called after calling this one.</p>
</section>

<section id="reset-form">
    <h2>resetForm</h2>

    <p><code>resetForm(resetFormData)</code> &mdash; Reset form. It hides all error elements and feedback icons. All the fields are marked as not validated yet.</p>

    <table class="table table-condensed">
        <thead>
            <tr>
                <th>Parameter</th>
                <th>Type</th>
                <th>Description</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>resetFormData</code></td>
                <td>Boolean</td>
                <td>If <code>true</code>, the method resets the fields which have validator rules.</td>
            </tr>
        </tbody>
    </table>

    <pre><code class="javascript">$(form).bootstrapValidator(options);
$(form).data('bootstrapValidator').resetForm();</code></pre>
</section>

<section id="set-live-mode">
    <h2>setLiveMode</h2>

    <p><code>setLiveMode(mode)</code> &mdash; Set live validating mode</p>

    <table class="table table-condensed">
        <thead>
            <tr>
                <th>Parameter</th>
                <th>Type</th>
                <th>Description</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>mode</code></td>
                <td>String</td>
                <td>Live validating mode. Can be <code>'enabled'</code>, <code>'disabled'</code>, <code>'submitted'</code></td>
            </tr>
        </tbody>
    </table>

    <div class="doc-example">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#livemode-form-tab" data-toggle="tab">Try it</a></li>
            <li><a href="#livemode-html-tab" data-toggle="tab">HTML</a></li>
            <li><a href="#livemode-js-tab" data-toggle="tab">JS</a></li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="livemode-form-tab">
                <form id="liveModeForm" class="form-horizontal">
                    <div class="form-group">
                        <div class="col-lg-offset-3 col-lg-9">
                            <button type="button" class="btn btn-success" data-mode="enabled">Set live="enabled"</button>
                            <button type="button" class="btn btn-success" data-mode="disabled">Set live="disabled"</button>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Text editors</label>
                        <div class="col-lg-5">
                            <input class="form-control" type="text" name="editors[]" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-3 col-lg-5">
                            <input class="form-control" type="text" name="editors[]" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-3 col-lg-5">
                            <input class="form-control" type="text" name="editors[]" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-3 col-lg-5">
                            <input class="form-control" type="text" name="editors[]" />
                        </div>
                    </div>
                </form>
            </div>

            <div class="tab-pane" id="livemode-html-tab">
                <pre><code class="xml">&lt;form id="liveModeForm" class="form-horizontal">
    &lt;div class="form-group">
        &lt;div class="col-lg-offset-3 col-lg-9">
            &lt;button type="button" class="btn btn-success" <span class="doc-example-note">data-mode="enabled"</span>>
                Set live="enabled"
            &lt;/button>
            &lt;button type="button" class="btn btn-success" <span class="doc-example-note">data-mode="disabled"</span>>
                Set live="disabled"
            &lt;/button>
        &lt;/div>
    &lt;/div>

    &lt;div class="form-group">
        &lt;label class="col-lg-3 control-label">Text editors&lt;/label>
        &lt;div class="col-lg-5">
            &lt;input class="form-control" type="text" name="editors[]" />
        &lt;/div>
    &lt;/div>
    &lt;div class="form-group">
        &lt;div class="col-lg-offset-3 col-lg-5">
            &lt;input class="form-control" type="text" name="editors[]" />
        &lt;/div>
    &lt;/div>
    &lt;div class="form-group">
        &lt;div class="col-lg-offset-3 col-lg-5">
            &lt;input class="form-control" type="text" name="editors[]" />
        &lt;/div>
    &lt;/div>
    &lt;div class="form-group">
        &lt;div class="col-lg-offset-3 col-lg-5">
            &lt;input class="form-control" type="text" name="editors[]" />
        &lt;/div>
    &lt;/div>
&lt;/form></code></pre>
            </div>

            <div class="tab-pane" id="livemode-js-tab">
                <pre><code class="xml javascript">&lt;script>
$(document).ready(function() {
    $('#liveModeForm')
        .bootstrapValidator({
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                'editors[]': {
                    validators: {
                        notEmpty: {
                            message: 'The editor names are required'
                        }
                    }
                }
            }
        })
        .find('button[data-mode]').click(function() {
            $('#liveModeForm')
                .data('bootstrapValidator')
                <span class="doc-example-note">.setLiveMode($(this).attr('data-mode'))</span>;
        });
});
&lt;/script></code></pre>
            </div>
        </div>
    </div>
</section>

<section id="update-element-status">
    <h2>updateElementStatus</h2>

    <p><code>updateElementStatus($field, status, validatorName)</code> &mdash; Update validating result of given element</p>

    <table class="table table-condensed">
        <thead>
            <tr>
                <th>Parameter</th>
                <th>Type</th>
                <th>Description</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>$field</code></td>
                <td>jQuery</td>
                <td>The field element</td>
            </tr>
            <tr>
                <td><code>status</code></td>
                <td>String</td>
                <td>Can be <code>NOT_VALIDATED</code>, <code>VALIDATING</code>, <code>INVALID</code> or <code>VALID</code></td>
            </tr>
            <tr>
                <td><code>validatorName</code></td>
                <td>String</td>
                <td>The validator name. If <code>null</code>, the method updates validity result for all validators</td>
            </tr>
        </tbody>
    </table>
</section>

<section id="update-status">
    <h2>updateStatus</h2>

    <p><code>updateStatus(field, status, validatorName)</code> &mdash; Update validator result for given field</p>

    <table class="table table-condensed">
        <thead>
            <tr>
                <th>Parameter</th>
                <th>Type</th>
                <th>Description</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>field</code></td>
                <td>String</td>
                <td>The field name</td>
            </tr>
            <tr>
                <td><code>status</code></td>
                <td>String</td>
                <td>Can be <code>NOT_VALIDATED</code>, <code>VALIDATING</code>, <code>INVALID</code> or <code>VALID</code></td>
            </tr>
            <tr>
                <td><code>validatorName</code></td>
                <td>String</td>
                <td>The validator name. If <code>null</code>, the method updates validity result for all validators</td>
            </tr>
        </tbody>
    </table>

    <p>This method is useful when you want to use BootstrapValidator with other plugins such as <a href="http://eternicode.github.io/bootstrap-datepicker/">Bootstrap Date Picker</a>,
        <a href="http://eonasdan.github.io/bootstrap-datetimepicker/">Boostrap Datetime Picker</a>, <a href="http://ivaynberg.github.io/select2/">Select2</a>, etc.</p>

    <p>By default, the plugin doesn't re-validate a field once it is already validated and marked as a valid one. When using with other plugins,
        the field value is changed and therefore need to be re-validated.</p>

    <section id="datepicker-example">
        <p>The following example describes how to re-validate a field which uses with <a href="http://eonasdan.github.io/bootstrap-datetimepicker/">Boostrap Datetime Picker</a>:</p>

        <link href="/vendor/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
        <style type="text/css">
        .form-horizontal .has-feedback .input-group .form-control-feedback {
            top: 0;
            right: -30px;
        }
        </style>

        <div class="doc-example">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#datepicker-form-tab" data-toggle="tab">Try it</a></li>
                <li><a href="#datepicker-html-tab" data-toggle="tab">HTML</a></li>
                <li><a href="#datepicker-js-tab" data-toggle="tab">JS</a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="datepicker-form-tab">
                    <form id="datetimeForm" class="form-horizontal">
                        <div class="form-group">
                            <label class="col-lg-3 control-label">DateTime Picker</label>
                            <div class="col-lg-5">
                                <div class="input-group date" id="datetimePicker">
                                    <input type="text" class="form-control" name="datetimePicker" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="tab-pane" id="datepicker-html-tab">
                    <pre><code class="html">&lt;link href="/vendor/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />

&lt;form id="datetimeForm" method="post" class="form-horizontal">
    ...
    &lt;div class="form-group">
        &lt;label class="col-lg-3 control-label">DateTime Picker&lt;/label>
        &lt;div class="col-lg-5">
            &lt;div class="input-group date" id="datetimePicker">
                &lt;input type="text" class="form-control" name="datetimePicker" />
                &lt;span class="input-group-addon">
                    &lt;span class="glyphicon glyphicon-calendar">&lt;/span>
                &lt;/span>
            &lt;/div>
        &lt;/div>
    &lt;/div>
    ...
&lt;/form></code></pre>
                </div>

                <div class="tab-pane" id="datepicker-js-tab">
                    <pre><code class="xml javascript">&lt;script src="/vendor/momentjs/moment.min.js">&lt;/script>
&lt;script src="/vendor/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js">&lt;/script>
&lt;script>
$(document).ready(function() {
    $('#datetimePicker').datetimepicker();

    $('#datetimeForm').bootstrapValidator({
        fields: {
            ...
            datetimePicker: {
                validators: {
                    notEmpty: {
                        message: 'The date is required and cannot be empty'
                    },
                    date: {
                        format: 'MM/DD/YYYY h:m A'
                    }
                }
            }
        }
    });

    $('#datetimePicker')
        .on('dp.change dp.show', function(e) {
            // Validate the date when user change it
            $('#datetimeForm')
                // Get the bootstrapValidator instance
                .data('bootstrapValidator')
                // Mark the field as not validated, so it'll be re-validated when the user change date
                <span class="doc-example-note">.updateStatus('datetimePicker', 'NOT_VALIDATED', null)</span>
                // Validate the field
                .validateField('datetimePicker');
        });
});
&lt;/script></code></pre>
                </div>
            </div>
        </div>
    </section>

    <section id="cc-example">
        <p>The <code>updateStatus</code> method might be used when a field validity is effected by other filed.</p>
        <p>For example, the form below asks user for credit card expiration. The expiration is valid if it is in the range of next month and next 10 year.</p>
        <div class="doc-example">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#cc-form-tab" data-toggle="tab">Try it</a></li>
                <li><a href="#cc-html-tab" data-toggle="tab">HTML</a></li>
                <li><a href="#cc-js-tab" data-toggle="tab">JS</a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="cc-form-tab">
                    <form id="paymentForm" class="form-horizontal">
                        <div class="form-group">
                            <label class="col-lg-3 control-label">Expiration</label>
                            <div class="col-lg-2">
                                <input type="text" class="form-control" placeholder="Month" name="expMonth" />
                            </div>
                            <div class="col-lg-2">
                                <input type="text" class="form-control" placeholder="Year" name="expYear" />
                            </div>
                        </div>
                    </form>
                </div>

                <div class="tab-pane" id="cc-html-tab">
                    <pre><code class="xml">&lt;form id="paymentForm" class="form-horizontal">
    &lt;div class="form-group">
        &lt;label class="col-lg-3 control-label">Expiration&lt;/label>
        &lt;div class="col-lg-2">
            &lt;input type="text" class="form-control" placeholder="Month" name="expMonth" />
        &lt;/div>
        &lt;div class="col-lg-2">
            &lt;input type="text" class="form-control" placeholder="Year" name="expYear" />
        &lt;/div>
    &lt;/div>
&lt;/form></code></pre>
                </div>

                <div class="tab-pane" id="cc-js-tab">
                    <pre><code class="xml javascript">&lt;script>
$(document).ready(function() {
    $('#paymentForm').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            expMonth: {
                validators: {
                    notEmpty: {
                        message: 'The expiration month is required'
                    },
                    digits: {
                        message: 'The expiration month can contain digits only'
                    },
                    callback: {
                        message: 'Expired',
                        callback: function(value, validator) {
                            value = parseInt(value, 10);
                            var year         = validator.getFieldElements('expYear').val(),
                                currentMonth = new Date().getMonth() + 1,
                                currentYear  = new Date().getFullYear();
                            if (value < 0 || value > 12) {
                                return false;
                            }
                            if (year == '') {
                                return true;
                            }
                            year = parseInt(year, 10);
                            if (year > currentYear || (year == currentYear && value > currentMonth)) {
                                <span class="doc-example-note">validator.updateStatus('expYear', 'VALID');</span>
                                return true;
                            } else {
                                return false;
                            }
                        }
                    }
                }
            },
            expYear: {
                validators: {
                    notEmpty: {
                        message: 'The expiration year is required'
                    },
                    digits: {
                        message: 'The expiration year can contain digits only'
                    },
                    callback: {
                        message: 'Expired',
                        callback: function(value, validator) {
                            value = parseInt(value, 10);
                            var month        = validator.getFieldElements('expMonth').val(),
                                currentMonth = new Date().getMonth() + 1,
                                currentYear  = new Date().getFullYear();
                            if (value < currentYear || value > currentYear + 10) {
                                return false;
                            }
                            if (month == '') {
                                return false;
                            }
                            month = parseInt(month, 10);
                            if (value > currentYear || (value == currentYear && month > currentMonth)) {
                                <span class="doc-example-note">validator.updateStatus('expMonth', 'VALID');</span>
                                return true;
                            } else {
                                return false;
                            }
                        }
                    }
                }
            }
        }
    });
});
&lt;/script></code></pre>
                </div>
            </div>
        </div>
    </section>
</section>

<section id="validate">
    <h2>validate</h2>

    <p><code>validate()</code> &mdash; Validate form manually. It is useful when you want to validate form by clicking a button or a link instead of a submit buttons.</p>

    <pre><code class="javascript">$(form).bootstrapValidator(options).bootstrapValidator('validate');

// or
$(form).bootstrapValidator(options);
$(form).data('bootstrapValidator').validate();</code></pre>
</section>

<section id="validate-field">
    <h2>validateField</h2>

    <p><code>validateField(field)</code> &mdash; Validate field.</p>

    <table class="table table-condensed">
        <thead>
            <tr>
                <th>Parameter</th>
                <th>Type</th>
                <th>Description</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>field</code></td>
                <td>String</td>
                <td>The field name</td>
            </tr>
        </tbody>
    </table>
</section>

<script src="/vendor/momentjs/moment.min.js"></script>
<script src="/vendor/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script>
$(document).ready(function() {
    // setLiveMode() example
    $('#liveModeForm')
        .bootstrapValidator({
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                'editors[]': {
                    validators: {
                        notEmpty: {
                            message: 'The editor names are required'
                        }
                    }
                }
            }
        })
        .find('button[data-mode]').click(function() {
            $('#liveModeForm').data('bootstrapValidator').setLiveMode($(this).attr('data-mode'));
        });

    // updateStatus() example
    $('#datetimePicker').datetimepicker();

    $('#datetimeForm').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            datetimePicker: {
                validators: {
                    notEmpty: {
                        message: 'The date is required and cannot be empty'
                    },
                    date: {
                        format: 'MM/DD/YYYY h:m A'
                    }
                }
            }
        }
    });

    $('#datetimePicker')
        .on('dp.change dp.show', function(e) {
            // Validate the date when user change it
            $('#datetimeForm')
                .data('bootstrapValidator')
                .updateStatus('datetimePicker', 'NOT_VALIDATED', null)
                .validateField('datetimePicker');
        });

    $('#paymentForm').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            expMonth: {
                validators: {
                    notEmpty: {
                        message: 'The expiration month is required'
                    },
                    digits: {
                        message: 'The expiration month can contain digits only'
                    },
                    callback: {
                        message: 'Expired',
                        callback: function(value, validator) {
                            value = parseInt(value, 10);
                            var year         = validator.getFieldElements('expYear').val(),
                                currentMonth = new Date().getMonth() + 1,
                                currentYear  = new Date().getFullYear();
                            if (value < 0 || value > 12) {
                                return false;
                            }
                            if (year == '') {
                                return true;
                            }
                            year = parseInt(year, 10);
                            if (year > currentYear || (year == currentYear && value > currentMonth)) {
                                validator.updateStatus('expYear', 'VALID');
                                return true;
                            } else {
                                return false;
                            }
                        }
                    }
                }
            },
            expYear: {
                validators: {
                    notEmpty: {
                        message: 'The expiration year is required'
                    },
                    digits: {
                        message: 'The expiration year can contain digits only'
                    },
                    callback: {
                        message: 'Expired',
                        callback: function(value, validator) {
                            value = parseInt(value, 10);
                            var month        = validator.getFieldElements('expMonth').val(),
                                currentMonth = new Date().getMonth() + 1,
                                currentYear  = new Date().getFullYear();
                            if (value < currentYear || value > currentYear + 10) {
                                return false;
                            }
                            if (month == '') {
                                return false;
                            }
                            month = parseInt(month, 10);
                            if (value > currentYear || (value == currentYear && month > currentMonth)) {
                                validator.updateStatus('expMonth', 'VALID');
                                return true;
                            } else {
                                return false;
                            }
                        }
                    }
                }
            }
        }
    });
});
</script>
