---
layout:           default
title:            remote validator
permalink:        /validators/remote/
previous_section: /validators/regexp/
next_section:     /validators/rtn/
comments:         true
---

<section class="doc-heading">
    <h1><a href="/validators/">Validators</a> / remote</h1>
    {% include edit-button.html %}
</section>

<section>
    <p>Perform remote checking via Ajax request.</p>

    <table class="table table-condensed">
        <thead>
            <tr>
                <th>Option</th>
                <th>Equivalent HTML attribute</th>
                <th>Type</th>
                <th>Description</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>message</code></td>
                <td><code>data-bv-remote-message</code></td>
                <td>String</td>
                <td>The error message</td>
            </tr>
            <tr>
                <td><code>url</code> (*)</td>
                <td><code>data-bv-remote-url</code></td>
                <td>String</td>
                <td>The remote URL that responses an encoded JSON of array containing the <code>valid</code> key</td>
            </tr>
            <tr>
                <td><code>name</code></td>
                <td><code>data-bv-remote-name</code></td>
                <td>String</td>
                <td>The name of field which need to validate</td>
            </tr>
            <tr>
                <td><code>data</code></td>
                <td>n/a</td>
                <td>Object</td>
                <td>The data sent to remote URL.<br />You don't need to use this option if there is only field, defined as field name, sent to the remote URL.</td>
            </tr>
        </tbody>
    </table>
</section>

<section id="name-option">
    <h2>name option</h2>
    <p>By default, it will be set as the name of field.</p>
    <p>You can override the <code>name</code> option by using the <code>data-bv-remote-name</code> attribute.</p>

    <p>Here are two cases which you might need to use this attribute.</p>

    <p class="doc-box-info">Use different name for same field on different forms:</p>
    <p>For example, the <i>Sign up</i> and <i>Profile</i> forms use the same back-end URL to validate the email address which is declared with different name:</p>
    <pre><code class="xml">&lt;!-- Signup form -->
&lt;form id="signupForm" class="form-horizontal">
    &lt;div class="form-group">
        &lt;label class="col-lg-3 control-label">Email&lt;/label>
        &lt;div class="col-lg-5">
            &lt;input type="text" class="form-control" <span class="doc-example-note">name="login" data-bv-remote-name="email"</span> />
        &lt;/div>
    &lt;/div>
&lt;/form>

&lt;!-- Edit profile form -->
&lt;form id="profileForm" class="form-horizontal">
    &lt;div class="form-group">
        &lt;label class="col-lg-3 control-label">Email&lt;/label>
        &lt;div class="col-lg-5">
            &lt;input type="text" class="form-control" <span class="doc-example-note">name="email" data-bv-remote-name="email"</span> />
        &lt;/div>
    &lt;/div>
&lt;/form></code></pre>

    <p class="doc-box-info">Multiple fields using the same back-end URL to validate</p>
    <pre><code class="xml">&lt;form id="profileForm" class="form-horizontal">
    &lt;div class="form-group">
        &lt;label class="col-lg-3 control-label">Primary email&lt;/label>
        &lt;div class="col-lg-5">
            &lt;input type="text" class="form-control"
                <span class="doc-example-note">name="primary_email" data-bv-remote-name="email"</span> />
        &lt;/div>

        &lt;label class="col-lg-3 control-label">Secondary email&lt;/label>
        &lt;div class="col-lg-5">
            &lt;input type="text" class="form-control"
                <span class="doc-example-note">name="secondary_email" data-bv-remote-name="email"</span> />
        &lt;/div>
    &lt;/div>
&lt;/form></code></pre>
</section>

<section id="data-option">
    <h2>data option</h2>
    <p>By default, it is set to <code>{ fieldName: fieldValue }</code> where <code>fieldName</code> and <code>fieldValue</code> are replaced with the field name and current value, respectively.</p>
    <p>In some cases, you might want to send more fields with dynamic values to the back-end. For instance, the registration form need to validate both the username and emails.</p>

    <p>The following sample code uses the <code>data</code> option showing how to do this:</p>
    <div class="doc-example">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#data-html-tab" data-toggle="tab">HTML</a></li>
            <li><a href="#data-js-tab" data-toggle="tab">JS</a></li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="data-html-tab">
                <pre><code class="xml">&lt;form id="registrationForm" class="form-horizontal">
    &lt;div class="form-group">
        &lt;label class="col-lg-3 control-label">Username&lt;/label>
        &lt;div class="col-lg-5">
            &lt;input type="text" class="form-control" name="username" />
        &lt;/div>
    &lt;/div>
    &lt;div class="form-group">
        &lt;label class="col-lg-3 control-label">Email&lt;/label>
        &lt;div class="col-lg-5">
            &lt;input type="text" class="form-control" name="email" />
        &lt;/div>
    &lt;/div>
    &lt;div class="form-group">
        &lt;label class="col-lg-3 control-label">Password&lt;/label>
        &lt;div class="col-lg-5">
            &lt;input type="password" class="form-control" name="password" />
        &lt;/div>
    &lt;/div>
&lt;/form></code></pre>
            </div>

            <div class="tab-pane" id="data-js-tab">
                <pre><code class="xml javascript">&lt;script>
$(document).ready(function() {
    $('#registrationForm').bootstrapValidator({
        fields: {
            username: {
                message: 'The username is not valid',
                validators: {
                    remote: {
                        url: '/path/to/backend/',
                        <span class="doc-example-note">data: function(validator) {
                            return {
                                email: validator.getFieldElements('email').val()
                            };
                        },</span>
                        message: 'The username is not available'
                    }
                }
            },
            email: {
                validators: {
                    remote: {
                        url: '/path/to/backend/',
                        <span class="doc-example-note">data: function(validator) {
                            return {
                                username: validator.getFieldElements('username').val()
                            };
                        },</span>
                        message: 'The email is not available'
                    }
                }
            }
        }
    });
});
&lt;/script></code></pre>
            </div>
        </div>
    </div>
</section>

{% include related.html %}
